#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

# Alvaro (bsources)
# Clase: bTransfer
# Version: 0.1

import sys

import ttk
import Tkinter
import tkFileDialog

import TcpManager

import threading

class bTransfer(Tkinter.Tk):

    # Callback para inyectar un mensaje al log
    def LogTextInyection(self, texto):
        
        self.RxLog.insert(Tkinter.END, texto);

    # Definimos las funciones de los botones
    def SeleccionarFichero(self):
    
        # Creamos un dialog para pedir esto
        self.fichero = tkFileDialog.askopenfilename()
        
        self.LogTextInyection("Fichero seleccionado para enviar: ")
        self.LogTextInyection(self.fichero)
        
    def EnviarFichero(self):
      
        # Tomamos los datos de la pantalla
        IP = self.IPText.get()
        puerto = self.PortText.get()
        
        # Enviamos el fichero
        Conexion = TcpManager.TcpManager()
                
        hilo = threading.Thread(target=Conexion.Enviar, args=(IP, puerto, 
                                  self.fichero, self.LogTextInyection))
        hilo.daemon = False
        hilo.start()
        
    def EncenderServer(self):
      
        try:
          
            # Tomamos los datos de la pantalla
            IP = self.IPText.get()
            puerto = self.PortText.get()
            
            # Enviamos el fichero
            Conexion = TcpManager.TcpManager()
                    
            hilo = threading.Thread(target=Conexion.CreateServer, args=(puerto, 
                                                    self.LogTextInyection))
            hilo.daemon = False
            hilo.start()
            
        except Exception as e:
            
            print(e)
            self.LogTextInyection(e)

    # Metodo de creacion de la clase
    def __init__(self, parent, titulo):
      
        Tkinter.Tk.__init__(self, parent, titulo)
        
        # Guardamos una referencia al padre para invocarlo
        self.parent = parent
        
        # Configuramos el titulo y el mensaje
        self.titulo = titulo
        
        # Inicializamos la interfaz
        self.initialize()
        
        # Bucle de espera de eventos
        self.mainloop()

    # Metodo de creacion de la Interfaz
    def initialize(self):
      
        # Cargamos el titulo (MutableString)
        self.title(self.titulo)
      
        # Creamos un layout para colocar los objetos dentro
        self.grid()
        
        # ZONA DE RECEPCI�N
        
        # Creamos el elemento donde va el texto de Rx
        self.textoRx = Tkinter.Label(self, anchor='center',
                                              text='Zona de Recepci�n')
        
        # A�adimos el elemento al layout manager - NO SE VE
        self.textoRx.grid(column=0, row=0, sticky='WE')
        
        # Creamos las 3 secciones donde se ven los ficheros en Rx, y el log de Rx
        self.Rx1Bar = ttk.Progressbar(self)
        self.Rx1Label = Tkinter.Label(self, anchor='center', text='Fichero 1')
        self.Rx1Label.grid(column=0, row=1, sticky="WE")
        self.Rx1Bar.grid(column=0, row=2, sticky="WE")
        
        self.Rx2Bar = ttk.Progressbar(self)
        self.Rx2Label = Tkinter.Label(self, anchor='center', text='Fichero 2')
        self.Rx2Label.grid(column=1, row=1, sticky="WE")
        self.Rx2Bar.grid(column=1, row=2, sticky="WE")
        
        self.Rx3Bar = ttk.Progressbar(self)
        self.Rx3Label = Tkinter.Label(self, anchor='center', text='Fichero 3')
        self.Rx3Label.grid(column=2, row=1, sticky="WE")
        self.Rx3Bar.grid(column=2, row=2, sticky="WE")
        
        # ZONA DE ENV�O
        
        self.Tx1Bar = ttk.Progressbar(self)
        self.Tx1Label = Tkinter.Label(self, anchor='center', text='Fichero a enviar')
        self.Tx1Label.grid(column=3, row=1, sticky="WE")
        self.Tx1Bar.grid(column=3, row=2, sticky="WE")
        
        # DATOS DE CONEXION
        
        self.ConnectionGroup = Tkinter.LabelFrame(self, text="Group", padx=5, pady=5)

        self.IPLabel = Tkinter.Label(self, anchor='center', text='IP Destino')
        self.IPLabel.grid(column=4, row=1, sticky="WE")
        
        self.IPText = Tkinter.Entry(self, text='0.0.0.0')
        self.IPText.grid(column=4, row=2, sticky="WE")
        
        self.PortLabel = Tkinter.Label(self, anchor='center', text='Puerto destino')
        self.PortLabel.grid(column=4, row=3, sticky="WE")
        
        self.PortText = Tkinter.Entry(self, text='65000')
        self.PortText.grid(column=4, row=4, sticky="WE")
        
        # LOG
        
        self.RxLog = Tkinter.Listbox(self)
        self.RxLog.grid(column=0, columnspan=4, row=3, sticky="WENS")
        
        # Habilitamos el reescalado automatico
        self.grid_columnconfigure(0, weight=1)
        self.grid_columnconfigure(1, weight=1)
        self.grid_columnconfigure(2, weight=1)
        self.grid_columnconfigure(3, weight=1)
        self.grid_columnconfigure(4, weight=1)
        
        self.grid_rowconfigure(0, weight=1)
        self.grid_rowconfigure(1, weight=1)
        self.grid_rowconfigure(2, weight=1)
        self.grid_rowconfigure(3, weight=1)
        self.grid_rowconfigure(4, weight=1)
        
        # Tanto en Horizontal como Vertical
        self.resizable(True, True)
        
        # Creamos el boton de ENVIAR y el de SELECCIONAR
        self.botSel = Tkinter.Button(self, anchor='center', 
                text='Elegir Fichero', command=self.SeleccionarFichero)
        self.botSel.grid(column=1, columnspan=1, row=5, sticky='WE')
        
        self.botSend = Tkinter.Button(self, anchor='center', 
                              text='Enviar', command=self.EnviarFichero)
        self.botSend.grid(column=2, columnspan=1, row=5, sticky='WE')
        
        # Boton de servidor
        self.botServer = Tkinter.Button(self, anchor='center', 
                          text='Servidor', command=self.EncenderServer)
        self.botServer.grid(column=3, columnspan=1, row=5, sticky='WE')
        
        
        # Y ahora que la ventana se ajuste al contenido
        # OJO con los textos largos!
        # self.update()
        # self.geometry(self.geometry())
        
        # Centramos en pantalla o en el padre la aparici�n
        if self.parent == None:
            self.Centrar(self)
        else:
            self.Centrar(self.parent)
            
    
    # Centramos la aparici�n
    def Centrar(self, win):
        
        # Pintamos con precisi�n, para obtener mejores valores
        win.update_idletasks()
        
        # Tomamos el ancho y alto de la ventana
        width = win.winfo_width()
        height = win.winfo_height()
        
        # Calculamos el centro
        x = (win.winfo_screenwidth() // 2) - (width // 2)
        y = (win.winfo_screenheight() // 2) - (height // 2)
        
        # Aplicamos al posicionamiento los c�lculos
        win.geometry('{}x{}+{}+{}'.format(width, height, x, y))
        
# Metodo de creacion del Main - Lo que se ejecuta si se llama al
# programa desde la linea de comandos
if __name__ == "__main__":
  
    ventana = bTransfer(None, 'bTransfer v0.1')
    
