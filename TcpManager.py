#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

# Alvaro (bsources)
# Clase: TcpManager
# Version: 0.1.1

import os
import sys
import socket
import Queue

import threading

class TcpManager():
  
    ReceiveStack = Queue.Queue()
  
    # Crea un servidor escuchando en el puerto aceptando hasta attend
    # conexiones simult�neas
    def CreateServer(self, puerto, showLog):
		
		TCP_CLIENTS_MAX = 3
		TCP_IP = ''
		if puerto == None:
			TCP_PORT = 8888
		else:
			TCP_PORT = int(puerto)

      
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.bind((TCP_IP, TCP_PORT))
        sock.listen(TCP_CLIENTS_MAX)
        
        showLog(" + Servidor escuchando en el puerto: " + TCP_PORT + " + ")
        
        # Bloqueo sincrono hasta que llega un cliente
        ClientSocket, ClientAddress = sock.accept()
        showLog(" - Cliente recibido, IP: " + ClientAddress)

        # Lanzamos la funci�n que lo trata en un hilo a parte
        hilo = threading.Thread(target=self.Attend, 
							args=(ClientSocket, ClientAddress, showLog))
        hilo.daemon = False
        hilo.start()
      
    # Recibe un fichero por socket
    def Attend(self, ClientSocket, ClientAddress, showLog):
      
		TCP_BUFFER = 512
      
        showLog("Conectando con cliente...")
        
        # 1� Recibimos nombre del fichero y tama�o
        datos = ClientSocket.recv(TCP_BUFFER)
        
        fichero, tamanio = datos.split(",")
        
        showLog("Abriendo fichero para escritura...")
        f = open(fichero, 'r+')
        
        try:
			tamanio, datos = tamanio.split("\n")
		except ValueError:
			# Significa que aun no estaba el \n
			pass
		else:
			f.write(datos)
		        
        # 1.5� Lo pasamos a los indicadores del stack
        showLog("Fichero: " + fichero)
        showLog("Tama�o: " + str(tamanio) + "MB")
        
        # 2� Recibimos el fichero en si
        while(datos):
        
            datos = ClientSocket.recv(TCP_BUFFER)
            
            # self.ReceiveStack.put(datos)
            f.write(datos)
            
            
        showLog("Fichero recibido...")
        ClientSocket.close()
        showLog("Conexi�n terminada...")
        f.close()
        showLog("Fichero escrito...")
        
        
        
    # Recibe un fichero por socket
    def Enviar(self, IP, puerto, fichero, showLog):
      
		TCP_BUFFER = 512
		
		if puerto == None:
			TCP_PORT = 8888
		else:
			TCP_PORT = int(puerto)
      
        showLog(" + Conectando con el servidor + ")
        showLog("IP: " + IP)
        showLog("Puerto: " + TCP_PORT)
        
        # 0� Creamos la conexi�n
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((IP, TCP_PORT))
        
        # 1� Enviamos nombre del fichero y tama�o
        showLog(" + Enviando datos del fichero...")
        
        tamanio = os.path.getsize(fichero) / 1024 / 1024
        
        nombre = os.path.basename(fichero)
        
        showLog("Nombre: " + fichero)
        showLog("Tama�o: " + str(tamanio) + "MB")
        
        sock.send(fichero + "," + str(tamanio) + "\n")
        
        # 2� Enviamos el fichero
        f = open(fichero, 'rb')
        datos = f.read(TCP_BUFFER)
        while(datos):
        
            datos = sock.send(datos)
            
            datos = f.read(TCP_BUFFER)
            
        
        sock.shutdown(socket.SHUT_WR)
        showLog("Fichero enviado...")
        # sock.close()
        showLog("Conexi�n terminada...")
    
    
    
    
    
